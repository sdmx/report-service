<?php

namespace App\Support\Category;

class EurostatAdapter {

    private $file;

    public function __construct() {
        $this->file = storage_path("app/public/category.txt");
    }

    public function findByKey($key = null, $depth = 1) {
        $fres = fopen($this->file, "r");
        $tab = "  ";
        $mp_len = null;
        $res = [];

        while (!feof($fres)) {
            $str = fgets($fres);
            $clean_str = preg_replace("~(^\s*|\n$)~", "", $str);

            if (strlen($clean_str) === 0) {
                continue;
            }

            $item = explode("//", $clean_str);
            preg_match_all("~$tab~", $str, $matches);
            $len = count($matches[0]);

            if ($key === null) {
                $mp_len = -1;
            }

            if ($mp_len !== null) {
                $diff_len = $len - $mp_len;

                if ($diff_len <= 0) {
                    break;
                }
                else if ($diff_len <= $depth) {
                    $res[] = [
                        "code" => preg_replace("~^(@|#)~", "", $item[0]),
                        "type" => $this->getTreeType($item[0]),
                        "name" => isset($item[1]) ? $item[1] : $item[0],
                        "level" => $len,
                    ];
                }
            }
            // main point
            else if (strpos($str, $key) !== false) {
                $mp_len = $len;
            }

        }

        fclose($fres);

        return $res;
    }

    private function getTreeType($name) {
        switch (substr($name, 0, 1)) {
            case "@": return "category";
            case "#": return "dataflow";
            default: return "label";
        }
    }
}